	// Loading Fontawesome file
document.write('<script src="https://kit.fontawesome.com/a661a1883d.js" crossorigin="anonymous"></script>');

// Moment JS for central time zone
document.write('<script src="https://momentjs.com/downloads/moment-with-locales.js" crossorigin="anonymous"></script>');
document.write('<script type="text/javascript" src="https://momentjs.com/downloads/moment-timezone-with-data.js"></script>');
document.write('<script defer src="https://unpkg.com/smoothscroll-polyfill@0.4.4/dist/smoothscroll.min.js"></script>')
// initial chat button that looks on page
$('a#prime').click(function(){
 $("#chat_frame").css({"display": "block !important", "animation": "zoomIn 0.2s cubic-bezier(0.42, 0, 0.58, 1)"});
});

$(window).load(function(){
	var date = new Date();
  	var current_day = moment().tz('America/Chicago').format('d');

  	if (current_day >= 1 && current_day <= 5){ // time slots for monday to friday
  	var startTime_slot_one = '00:00:00';var endTime_slot_one = '23:00:00';var startTime_slot_two = '23:00:00';var endTime_slot_two = '23:59:59';
	}else if (current_day == 6){ // time slots for saturday
  	var startTime_slot_one = '00:00:00';var endTime_slot_one = '23:00:00'; var startTime_slot_two = '23:00:00';var endTime_slot_two = '23:59:59';
	}else if (current_day == 0){ // time slots for sunday
  	var startTime_slot_one = '00:00:00';var endTime_slot_one = '23:00:00'; var startTime_slot_two = '23:00:00';var endTime_slot_two = '23:59:59';
 	}

	// // fetching current central time zone
	current = moment().tz('America/Chicago').format('HH:mm:ss');
	valid = (startTime_slot_one < current && endTime_slot_one > current)||(startTime_slot_two < current && endTime_slot_two > current)  
	// show chat section to user if current time is betwwen start & end time  
	if (valid == true){
    	$('body').append('<a style="background:#0c0622;color:#fff;border-radius:50px;height:56px;width:56px;position: fixed;z-index: 999999;bottom:28px;right:12px;" id="prime"><i style="font-size:20px;padding:19px 18px;text-align:center;width: 56px;height: 56px;" onclick="myFunction(this)" class="fas fa-comment-alt"></i></a>');
    	$('body').append('<a id="text_chat" style="display:block;padding:15px;background:#fff;color:#000;border-radius:12px 12px 0 12px;height:58px;width:260px;position: fixed;z-index: 999999;bottom:50px;right:100px;box-shadow:0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"> Self-help TV troubleshooting.</div>');
    	var file_src = "https://vizio.simplifyreality.com/";
    	$('<iframe name="Right" id="chat_frame" style="border:0;background:#ccc;height:498px;width:370px;bottom:30px;right:48px;box-shadow:1px 1px 100px 2px rgb(0 0 0 / 22%);display:none;border-radius:15px;position:fixed;z-index:99999;margin-right: 20px;margin-bottom: 30px;"></iframe>')
    	.attr('src',file_src)
    	.attr('height',498)
    	.attr('width',370)
    	.appendTo('body');

    	// Toggle chat section when opens
    	$('a#prime').click(function(){
		$("#chat_frame").toggle();
		$("#text_chat").toggle();

		});

		// code Responsive chat window
		var l0 = window.matchMedia( "(min-width: 300px)" );
		var l1 = window.matchMedia( "(min-width: 350px)" );
		var l2 = window.matchMedia( "(min-width: 500px)" );
	    var l3 = window.matchMedia( "(min-width: 700px)" );
	     if (l3.matches) {
			document.getElementById("chat_frame").style.width = "370px";
		} else if (l2.matches){
			document.getElementById("chat_frame").style.width = "85%";
			document.getElementById("chat_frame").style.bottom = "71px";
			document.getElementById("chat_frame").style.right = "10%";
			document.getElementById("chat_frame").style.height = "80%";

		} else if (l1.matches){
			document.getElementById("chat_frame").style.width = "80%";
			document.getElementById("text_chat").style.right = "20%";
			document.getElementById("chat_frame").style.height = "85%";
		}else if (l0.matches){
			document.getElementById("chat_frame").style.width = "75%";
			document.getElementById("text_chat").style.right = "10%";
			document.getElementById("text_chat").style.bottom = "20%";
		}
    }

     // height: 600px, width: 350px
	 
});

// change fontawesome icon when chat section opens
function myFunction(x) {
  x.classList.toggle("fa-times");

}